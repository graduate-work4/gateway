import { Body, Controller, Get, Inject, OnModuleInit, Post, Put, Req, Res } from '@nestjs/common';
import { AUTH_SERVICE_NAME, AuthRequest, AuthServiceClient } from './auth.pb';
import { ClientGrpc } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { AuthDto } from './auth.dto';

@Controller('auth')
export class AuthController implements OnModuleInit {
  private svc: AuthServiceClient;

  @Inject(AUTH_SERVICE_NAME)
  private readonly client: ClientGrpc;

  onModuleInit(): void {
    this.svc = this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  @Post('registration')
  private async registration(@Body() body: AuthDto,@Res({passthrough: true})res) {
    const data = await firstValueFrom( this.svc.registration(body))
    res.cookie('refreshToken', data.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true});
    delete data.refreshToken
    return  data
  }

  @Post('login')
  private async login(@Body() body: AuthDto,@Res({passthrough: true})res) {
    const data = await firstValueFrom( this.svc.login(body))
    res.cookie('refreshToken', data.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true});
    delete data.refreshToken
    return  data
  }

  @Get('refresh')
  private async refresh(@Req() request,@Res({passthrough:true})response) {
    const {refreshToken} = request.cookies;
    const data = await firstValueFrom(this.svc.refresh({refreshToken}))
    response.cookie('refreshToken', data.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true});
    return {token:data.accessToken,status:data.status,error:data.error}
  }

  @Put('logout')
  async logout(@Req()request, @Res({passthrough: true})response) {
    const {refreshToken} = request.cookies;
    response.clearCookie('refresh_token');
    await this.svc.logout(refreshToken)
  }
}
