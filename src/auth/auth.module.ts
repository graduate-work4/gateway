import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AUTH_PACKAGE_NAME, AUTH_SERVICE_NAME } from './auth.pb';

@Module({
  imports:[ClientsModule.register([
    {
      name:AUTH_SERVICE_NAME,
      transport:Transport.GRPC,
      options:{
        url:'0.0.0.0:50052',
        package:AUTH_PACKAGE_NAME,
        protoPath:'node_modules/gprc-node-proto/proto/auth.proto'
      }
    }
  ])],
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule {}
