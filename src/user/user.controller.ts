import { Controller, Get, Inject, OnModuleInit, Param } from '@nestjs/common';
import { AUTH_SERVICE_NAME, AuthServiceClient } from '../auth/auth.pb';
import { ClientGrpc } from '@nestjs/microservices';
import { USER_SERVICE_NAME, UserServiceClient } from './user.pb';
import { firstValueFrom } from 'rxjs';

@Controller('user')
export class UserController implements OnModuleInit {
  private svc: UserServiceClient;

  @Inject(USER_SERVICE_NAME)
  private readonly client: ClientGrpc;

  onModuleInit(): void {
    this.svc = this.client.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  @Get(':id')
  async getUser(@Param('id')id: string) {
    const data =  await firstValueFrom(this.svc.getUserById({ userId: +id, status: 0, error: null}));
    data.userId = +data.userId
    data.roles = data.roles.map((e)=>{
      return {description:e.description,id:+e.id,value:e.value}
    })
    return data
  }
}
