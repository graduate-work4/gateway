/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "user";

export interface CreateRoleRequest {
  value: string;
  description: string;
}

export interface CreateRoleResponse {
  id: number;
  error: string[];
  status: number;
}

export interface GetRolesRequest {
}

export interface CreateUserRequest {
  phone: string;
  password: string;
}

export interface GetUserByIdRequest {
  userId: number;
  error: string[];
  status: number;
}

export interface GetUserByIdResponse {
  userId: number;
  roles: Roles[];
  error: string[];
  status: number;
}

export interface LoginUserResponse {
  id: number;
  error: string[];
  status: number;
  phone: string;
  password: string;
}

export interface CreateUserResponse {
  id: number;
  error: string[];
  status: number;
}

export interface GetRolesResponse {
  roles: Roles[];
  error: string[];
  status: number;
}

export interface Empty {
  error: string[];
  status: number;
}

export interface GetUserForCheck {
  phone: string;
}

export interface Roles {
  id: number;
  value: string;
  description: string;
}

export interface AddRoleAndRemoveRequest {
  userId: number;
  role: string;
}

export const USER_PACKAGE_NAME = "user";

export interface RoleServiceClient {
  createRole(request: CreateRoleRequest): Observable<CreateRoleResponse>;

  getRoles(request: GetRolesRequest): Observable<GetRolesResponse>;
}

export interface RoleServiceController {
  createRole(
    request: CreateRoleRequest,
  ): Promise<CreateRoleResponse> | Observable<CreateRoleResponse> | CreateRoleResponse;

  getRoles(request: GetRolesRequest): Promise<GetRolesResponse> | Observable<GetRolesResponse> | GetRolesResponse;
}

export function RoleServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["createRole", "getRoles"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("RoleService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("RoleService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const ROLE_SERVICE_NAME = "RoleService";

export interface UserServiceClient {
  createUser(request: CreateUserRequest): Observable<CreateUserResponse>;

  getUser(request: CreateUserRequest): Observable<CreateUserResponse>;

  getUserForLogin(request: GetUserForCheck): Observable<LoginUserResponse>;

  getUserById(request: GetUserByIdRequest): Observable<GetUserByIdResponse>;

  addRoleToUser(request: AddRoleAndRemoveRequest): Observable<Empty>;

  removeRoleToUser(request: AddRoleAndRemoveRequest): Observable<Empty>;
}

export interface UserServiceController {
  createUser(
    request: CreateUserRequest,
  ): Promise<CreateUserResponse> | Observable<CreateUserResponse> | CreateUserResponse;

  getUser(
    request: CreateUserRequest,
  ): Promise<CreateUserResponse> | Observable<CreateUserResponse> | CreateUserResponse;

  getUserForLogin(
    request: GetUserForCheck,
  ): Promise<LoginUserResponse> | Observable<LoginUserResponse> | LoginUserResponse;

  getUserById(
    request: GetUserByIdRequest,
  ): Promise<GetUserByIdResponse> | Observable<GetUserByIdResponse> | GetUserByIdResponse;

  addRoleToUser(request: AddRoleAndRemoveRequest): Promise<Empty> | Observable<Empty> | Empty;

  removeRoleToUser(request: AddRoleAndRemoveRequest): Promise<Empty> | Observable<Empty> | Empty;
}

export function UserServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      "createUser",
      "getUser",
      "getUserForLogin",
      "getUserById",
      "addRoleToUser",
      "removeRoleToUser",
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("UserService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("UserService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const USER_SERVICE_NAME = "UserService";
